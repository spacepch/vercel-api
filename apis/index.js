import request from "../utils/request.js";

/**
 * 获取QQ用户信息接口
 * @param {*} uid qq账号
 * @returns Promise对象
 */
export const getqqAPI = (uid) => {
  return Promise.all([
    request({
      url: `https://api.usuuu.com/qq/${uid}`, // 400
    }),
    request({
      url: "https://zy.xywlapi.cc/qqapi", //500
      params: { qq: uid },
    }),
  ]);
};

/**
 * 获取B站用户信息
 * @param {*} uid B站uid
 * @returns Promise对象
 */
export const getbiliAPI = (uid) => {
  return request({
    url: `https://api.bilibili.com/x/web-interface/card`,
    params: { mid: uid },
    headers: {},
  });
};

/**
 * 获取github用户信息
 * @param {*} uid github用户名
 * @returns Promise对象
 */
export const getGithubAPI = (uid) => {
  return request({
    url: `https://api.github.com/users/${uid}`,
    params: {},
    headers: {},
  });
};