import axios from "axios";

const myAxios = axios.create({
  baseURL: "",
});

// 请求拦截器
// myAxios.interceptors.request.use(function (config) {
//   config.headers = {};
//   return config;
// });

export default myAxios;
