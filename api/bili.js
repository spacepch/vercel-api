import { getbiliAPI } from "../apis/index.js";
import store from "./_store.js";

export default async function handler(request, response) {
  response.setHeader("Access-Control-Allow-Origin", "*");
  const uid = request.query.keyword;
  if (uid == store.mybili) return response.send(store.error);
  if (!uid) return response.send(store.error);
  const { data: res } = await getbiliAPI(uid);
  if (res.code) return response.send(store.error);
  const data = {
    avatar: res.data.card.face,
    name: res.data.card.name,
    sex: res.data.card.sex,
    level: res.data.card.level_info.current_level,
    fans: res.data.card.fans,
    isFollow: res.data.following,
    sign: res.data.card.sign,
    vip: res.data.card.vip.type,
  };
  response.send(data);
}
