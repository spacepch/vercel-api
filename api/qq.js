import { getqqAPI } from "../apis/index.js";
import store from "./_store.js";

export default async function handler(request, response) {
  response.setHeader("Access-Control-Allow-Origin", "*");
  const uid = request.query.keyword;
  if (uid == store.myqq) return response.send(store.error);
  if (!uid) return response.send(store.error);
  const res = await getqqAPI(uid);
  if (res[0].data.code == 400) return response.send(store.error);
  const data = {
    qq: uid,
    name: res[0].data.data.name,
    avatar: res[0].data.data.avatar,
    phone: res[1].data.qq ? res[1].data.phone : "未绑定手机号",
    phonediqu: res[1].data.qq ? res[1].data.phonediqu : "未绑定手机号",
  };
  response.send(data);
}
