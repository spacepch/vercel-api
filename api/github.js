import { getGithubAPI } from "../apis/index.js";
import store from "./_store.js";

export default async function handler(request, response) {
  response.setHeader("Access-Control-Allow-Origin", "*");
  const keyword = request.query.keyword;
  if (keyword == store.mygithub) return response.send(store.error);
  if (!keyword) return response.send(store.error);
  const { data: res } = await getGithubAPI(keyword);
  if (res.message) return response.send(store.error);
  const data = {
    login: res.login,
    id: res.id,
    href: res.html_url,
    avatar: res.avatar_url,
    name: res.name,
    fans: res.followers,
    following: res.following,
  };
  response.send(data);
}
